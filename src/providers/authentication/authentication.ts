import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
// import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { HTTP } from '@ionic-native/http';

/*
  Generated class for the AuthenticationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthenticationProvider {
  apiUrl = 'http://linked-things-orgs.eu-gb.mybluemix.net/api/v1/users/';
  apiUrlorgLevel = 'http://linked-things-orgs.eu-gb.mybluemix.net/api/v1/';

  constructor(public http: HTTP) {
    console.log('Hello AuthenticationProvider Provider');
  }
  signup(data) {
    return new Promise((resolve, reject) => {

      this.http.post(this.apiUrl + "signup", data,{})
        .then(data => {
          resolve(data);
        }, (err) => {
          console.log(err.error)
        });
    });
  }
  signIn(data) {

    return new Promise((resolve, reject) => {

      this.http.post(this.apiUrl + "signin", data,{})
        .then(data => {
          resolve(data);
        }, (err) => {
          console.log(err.error)
        });
    });
  }
  authenticated () {
    let myObj = localStorage.getItem("addDevices");
    return myObj
  }
}
