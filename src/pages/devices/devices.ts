import { Component, OnDestroy } from '@angular/core';
import { NavController, NavParams, ModalController, Platform, ToastController } from 'ionic-angular';
import { OfficedevicesPage } from '../officedevices/officedevices';
import { AppliancePage } from '../appliance/appliance';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AddDevicesPage } from '../add-devices/add-devices';
import { DrcodereaderPage } from '../qrcode/drcodereader/drcodereader';
import { HTTP } from '@ionic-native/http';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Hotspot } from '@ionic-native/hotspot';

declare var WifiWizard;


@Component({
  selector: 'page-devices',
  templateUrl: 'devices.html',
})
export class DevicesPage {
  id: any;
  pass: any;
  str: any;

  data_hierarchy: any;
  organization: any;
  level: any;
  devices = 'https://linked-things-events.eu-gb.mybluemix.net/api/v1/levels/';
  _id: any;
  events: any;
  check: any;
  samples: Array<any> = [];
  selectedProduct: any;

  constructor(private toast: ToastController,private hotspot: Hotspot, private barcodeScanner: BarcodeScanner,platform: Platform,private http: HTTP,public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams) {
    // this.data_hierarchy = this.navParams.data
    // console.log(this.data_hierarchy);
    // console.log(this.navParams.data);

    WifiWizard.getCurrentSSID(ssidHandler, fail)

    function ssidHandler(s) {
      console.log("Current SSID" + s);
      // JSON.stringify(localStorage.setItem("ssid",s))
      localStorage.setItem("ssid", JSON.parse(s))
    } function fail(e) {
      alert("Failed" + e);
    }

  }

  go() {
    const modal = this.modalCtrl.create(AddDevicesPage);
    modal.present();
    // this.navCtrl.push(IframePage)
  }
  wifi() {
    /*     this.getSsidName();
     */
  }
  ngOnInit() {
    this.check = this.navParams.data
    console.log(this.check.length);
    // this.events = JSON.parse(localStorage.getItem('eventc'))
    console.log(this.events);
    /*     for (var i = 0; i < this.events.length; i++) {
          console.log(this.events)
          var ismatch = false;
          for (var j = 0; j < this.check.length; j++) {
            console.log(this.check)
    
            if (this.events[i].deviceId == this.check[j]._id) {
              ismatch = true;
              console.log(this.check)
    
              this.samples.push(this.events[i])
              console.log(this.samples)
    
              localStorage.setItem("even", JSON.stringify(this.samples))
              break;
              // console.log(this.newArray)
    
            }
          }
        
        } */
    console.log(this.samples);
  }

  showsubLevel(data_hierarchy) {
    this._id = data_hierarchy._id
    console.log(this._id)

    if (data_hierarchy['levels']) {
      console.log(data_hierarchy['levels']);
      this.navCtrl.push(AppliancePage, data_hierarchy['levels'])
    } else if (data_hierarchy['devices']) {
      console.log(data_hierarchy['devices']);
      this.navCtrl.push(AppliancePage, data_hierarchy['devices'])
    }
    else {
      console.log(data_hierarchy);
      setTimeout( this.navCtrl.push(AppliancePage, data_hierarchy), 300);
     
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad DevicesPage');
  }

}
