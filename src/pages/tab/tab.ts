import { Component } from '@angular/core';
import {NavController, NavParams } from 'ionic-angular';
import { MainPage } from '../main/main';



@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {

  data_hierarchy: any;
  sec: any;
  name: any;
  _id: any;
  created: any;
  createdBy: any;
  level: any;
  levelId: any;
  levelsLimit: any;
  schema: any;
  updated: any;
  type: any;
  _rev: any;
  devices: any;
  number: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

    this.data_hierarchy = this.navParams.data
    console.log(this.data_hierarchy);
    // this.name = this.data_hierarchy;
            // this.name = this.navParams.data
            // this._id = this.navParams.data['_id'];
            // this.created = this.navParams.data['created'];
            // this.createdBy = this.navParams.data['createdBy'];
            // this.level = this.navParams.data['level'];
            // this.levelId = this.navParams.data['levelId'];
            // this.levelsLimit = this.navParams.data['levelsLimit'];
            // this.name = this.navParams.data['name'];
            // this.schema = this.navParams.data['schema'];
            // this.type = this.navParams.data['type'];
            // this.updated = this.navParams.data['updated'];
            // this._rev = this.navParams.data['_rev'];
    this.devices =  this.data_hierarchy
    if(this.devices.length > 0){
      // this.devices 
            console.log(  this.devices );
     
    }else {
        this._id = this.navParams.data['_id'];
            this.created = this.navParams.data['created'];
            this.createdBy = this.navParams.data['createdBy'];
            this.level = this.navParams.data['level'];
            this.levelId = this.navParams.data['levelId'];
            this.levelsLimit = this.navParams.data['levelsLimit'];
            this.name = this.navParams.data['name'];
            this.schema = this.navParams.data['schema'];
            this.type = this.navParams.data['type'];
            this.updated = this.navParams.data['updated'];
            this._rev = this.navParams.data['_rev'];
    } 
    
  }
  showsubLevel(data_hierarchy){
    console.log(data_hierarchy);
    this.sec =data_hierarchy
    this.navCtrl.push(MainPage,this.sec)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabPage');
  }
  sendEsp(){
    const user = {
      _id: this._id,
      number: this.number,
  
      // imgPath:this.imgPaths
    }
    console.log(user)
    // var host = "*"
    var win = document.getElementsByTagName('iframe')[0];
    win.contentWindow.postMessage({ user }, "*")
  }
}
