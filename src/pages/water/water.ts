import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Hotspot } from '@ionic-native/hotspot';

@Component({
  selector: 'page-water',
  templateUrl: 'water.html',
})
export class WaterPage {
  _id: any;
  password: any;
  ssid: any;
  isActive: boolean = false;
  isAct: boolean = false;
  visability;
  items: any;
  visabili: any;
  OHMotorselect: any;
  UGMotorselect: any;
  lenghtOh: any;
  lenghtUG: any;
  widthOh: any;
  widthUG: any;
  heightOH: any;
  heightUG: any;
  orgid: string;
  id: string;
  pass: string;
  mac_Type: string;
  Type: string;

  constructor(private hotspot: Hotspot,public navCtrl: NavController, public navParams: NavParams) {
    let ssid = localStorage.getItem('ssid')
    let org_id = localStorage.getItem('org_id')
    let mac_Type= localStorage.getItem("mac_Type")
    this._id = ssid
    this.orgid = 
    this.Type =mac_Type
    console.log(this.Type );
  }
  change() {
    this.visability = true;
  }
  datachanged(e: any) {
    console.log(e);
    this.visability = e.checked

  }
  datachang(e: any) {
    console.log(e);
    this.visabili = e.checked

  }
  OHMotor(e) {
    console.log(e);
    this.OHMotorselect = e.checked
  }
  UGMotor(e) {
    console.log(e);
    this.UGMotorselect = e.checked
  }
  onChange(items) {
    console.log('onChange', items);
    let isChecked = false;
    for (let i = 0; i < items.length; i++) {
      if (items[i].checkBox == true) {
        isChecked = true;
        this.visability = true;
      }
    }
    if (isChecked == false) {
      this.visability = false;
    }
  }
  ionViewDidLoad() {
    this.id=   localStorage.getItem("espwifissid")
    this.pass= localStorage.getItem("espwifipass")

    this.hotspot.connectToWifiAuthEncrypt(this.id,  this.pass,"OPEN",["CCMP","TKIP","WEP40","WEP104"]).then((data)=>{
      console.log('Connected to User');
      
      // display alert here
      alert(data)
      }).catch(err =>{console.log(err)});
    console.log('ionViewDidLoad WaterPage');
  }
  submit() {
    let ssid = localStorage.getItem('ssid')
    const user = {
      _id: this._id,
      password: this.password,

      // imgPath:this.imgPaths
    }
    console.log(user)
    // var host = "*"

    window.parent.postMessage({ user }, "https://statics-7d7ea.firebaseapp.com/")
    console.log('received a message!');
  }
  wotorSubmit() {
    var watorform = {
      ssid :this._id ,
      password: this.password,
      org_id:this.orgid,
      oh: this.visability,
      lenghtOh: this.lenghtOh,
      widthOh: this.widthOh,
      heightOH: this.heightOH,
      UG: this.visabili,
      lenghtUG: this.lenghtUG,
      widthUG: this.widthUG,
      heightUG: this.heightUG,
      OHMotorselect: this.OHMotorselect,
      UGMotorselect: this.UGMotorselect,
    }
    var win = document.getElementsByTagName('iframe')[0];
    win.contentWindow.postMessage({ watorform }, "*")
    console.log(watorform)
  }

  //    mes() {
  //     var result = document.getElementById("textbox").value;
  //     var res = document.getElementById("text").value;

  //     console.log(result, 'received a message!');
  //     var host = "*"

  //     window.parent.postMessage({ result, res }, host)
  //     console.log('received a message!');


  // } window.onload = mes();
}
