import { Component } from '@angular/core';
import { NavController, NavParams, Platform ,ModalController} from 'ionic-angular';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Hotspot, HotspotNetwork } from '@ionic-native/hotspot';
import { IframePage } from '../../iframe/iframe';
import { AddDevicesPage } from '../../add-devices/add-devices';

declare var WifiWizard;

@Component({
  selector: 'page-drcodereader',
  templateUrl: 'drcodereader.html',
})
export class DrcodereaderPage {
  products: any[] = [];
  selectedProduct: any;
  productFound: boolean = false;
  str;
  id;
  pass;
  e;
  ssid;
  currentSSID;

  constructor(platform: Platform, private hotspot: Hotspot, private barcodeScanner: BarcodeScanner,
    public navCtrl: NavController, public navParams: NavParams, public modalCtrl: ModalController) {
      this.scan();
    // WifiWizard.getCurrentSSID((ssid: string) => alert(`Your SSID: ${ssid}`), this.errorHandler);

    // this.getCurrentSSID();
    // console.log(this.ssid)
    /*   function ssidHandler(s) {
        alert("Current SSID"+s);
    }
    
    function fail(e) {
        alert("Failed"+e);
    } */

    /*  function getCurrentSSID() {
         WifiWizard.getCurrentSSID(ssidHandler, fail);
     }  */
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      WifiWizard.getCurrentSSID(ssidHandler, fail)

      function ssidHandler(s) {
        console.log("Current SSID" + s);
        // JSON.stringify(localStorage.setItem("ssid",s))
        localStorage.setItem("ssid", JSON.parse(s))
      } function fail(e) {
        alert("Failed" + e);
      }
    });
  }



  /*  errorHandler(err: any) {
     alert(`Problem: ${err}`);
   }
 
    getSsidName() {
     wifiwizard.getCurrentSSID((ssid: string) => alert(`Your SSID: ${ssid}`), this.errorHandler);
   }
 
   isWifiEnabled() {
     wifiwizard.isWifiEnabled(truthy => alert(`Wifi Enabled: ${truthy}`), this.errorHandler);
   }
 
   listNetworks() {
     wifiwizard.listNetworks(networks => alert(`Networks: ${networks}`), this.errorHandler);
   }
  */




  ionViewDidLoad() {
    /*  WifiWizard.getCurrentSSID().then(data =>{
       this.ssid =data
       console.log(this.ssid)
     }).catch(err => {console.log(err)});
 
     console.log('ionViewDidLoad DrcodereaderPage'); */
  }
  go() {
    const modal = this.modalCtrl.create(AddDevicesPage);
    modal.present();
    // this.navCtrl.push(IframePage)
  }
  wifi() {
    /*     this.getSsidName();
     */
  }
  scan() {
    // this.selectedProduct = {};
    this.barcodeScanner.scan().then((barcodeData) => {

      this.selectedProduct = barcodeData;

      console.log(this.selectedProduct.text.split(','))
      this.str = this.selectedProduct.text.split(',')

      var ssid = this.str[0].toString()
      var ssps = this.str[1].toString()
      this.id = atob(ssid)
      this.pass = atob(ssps)
      console.log(this.id);
      console.log(this.pass);

      const espwifi = {
        id: this.id,
        pass: this.pass
      }
      localStorage.setItem("espwifissid", this.id)
      localStorage.setItem("espwifipass", this.pass)

      this.hotspot.connectToWifiAuthEncrypt(this.id, this.pass, "OPEN", ["CCMP", "TKIP", "WEP40", "WEP104"]).then((data) => {
        console.log('Connected to User');
        this.go()
        // display alert here
        alert(data)
      }).catch(err => { console.log(err) });
      // WifiWizard.removeNetwork(s);

      // WifiWizard.connectNetwork(this.id, this.pass, this.faile);

      console.log("password", this.id, this.pass);
      /*  this.hotspot.removeWifiNetwork(ssid).then( () => {
            this.hotspot.connectToWifi(this.id, this.pass) 
                .then((data) => {
                      console.log(".........hotspot..........",data);
                 }, (error) => {
                      console.log(".........hotspot..........",error);
                 });
       }, () => {
       }); */
      // WifiWizard.connectNetwork(this.id, this.pass,this.win,this.faile)
      // this.getCurrentSSID();
      // .then(data =>{console.log(data)}).catch(err => {console.log(err)});
      // this.hotspot.removeWifiNetwork(this.ssid).then(data =>{console.log(data)}).catch(err => {console.log(err)});
      // this.hotspot.connectToWifi(this.id, this.pass).then(data =>{console.log(data)}).catch(err => {console.log(err)});
      /*    WifiWizard.addNetwork(wifi.id,wifi.pass).then(data =>{
           console.log(data)
         }).catch(err => console.log(err)); */
      /*   this.hotspot.removeWifiNetwork(this.id).then( () => {
          this.hotspot.connectToWifi(this.id, this.pass) 
              .then((data) => {
                    console.log(data);
               }, (error) => {
                    console.log(error);
               });
     }, (error) => {
      console.log(error);
  }); */
      /*   this.hotspot.connectToWifi(this.id, this.pass).then( conn =>{
          console.log(conn)
        }).catch(err =>{
          console.log(err)
  
        }) */
      /* this.hotspot.addWifiNetwork(this.id,"Open", this.pass).then( conn =>{
        console.log(conn)
      }).catch(err =>{
        console.log(err)

      }) */

      // var ssid = str[1].toString()
      //   if(this.selectedProduct !== undefined) {
      //     this.productFound = true;
      //   } else {
      //     this.productFound = false;
      //     this.toast.show(`Product not found`, '5000', 'center').subscribe(
      //       toast => {
      console.log(this.selectedProduct);


      //       }
      //     );
      //   }
      // }, (err) => {
      //   this.toast.show(err, '5000', 'center').subscribe(
      //     toast => {
      //       console.log(toast);
      //     }
      //   );
    });
  }
  faile(e) {
    alert("Failed" + e);
  }
  win(e) {
    alert("Failed" + e);
  }
}
