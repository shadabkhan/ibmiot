import { Component, ViewChild } from '@angular/core';
import {  NavController, NavParams, Nav } from 'ionic-angular';
import { HomePage } from '../home/home';
import { MainPage } from '../main/main';
import { TabPage } from '../tab/tab';



@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {
  data_hierarchy: any;
  data: any;
  LEVELS: any;
  lev: any;
  LEVEL2: any;
  LEVEL1: any;
  _id: any;
  name: any;
  keys: string[];
  sec: any;
  devices: any[];
  connected: object;
  created: any;
  createdBy: any;
  level: any;
  levelId: any;
  levelsLimit: any;
  schema: any;
  type: any;
  updated: any;
  _rev: any;


  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.data_hierarchy = this.navParams.data

    console.log(  this.data_hierarchy );
    this.devices =  this.data_hierarchy
    if(this.devices.length > 0){
      // this.devices 
            console.log(  this.devices );
     
    }else {
      // this.name = this.data_hierarchy;
            // this.name = this.navParams.data
            this._id = this.navParams.data[0]['_id'];
            this.created = this.navParams.data[0]['created'];
            this.createdBy = this.navParams.data[0]['createdBy'];
            this.level = this.navParams.data[0]['level'];
            this.levelId = this.navParams.data[0]['levelId'];
            this.levelsLimit = this.navParams.data[0]['levelsLimit'];
            this.name = this.navParams.data[0]['name'];
            this.schema = this.navParams.data[0]['schema'];
            this.type = this.navParams.data[0]['type'];
            this.updated = this.navParams.data[0]['updated'];
            this._rev = this.navParams.data[0]['_rev'];
            this.connected 
            // console.log(this.connected)

    }
    // if( !this.data_hierarchy) {
    //   this.name = this.navParams.data['name']
    //   console.log( this.name  );
    // }
    // this.LEVELS =  Object.assign({}, this.data_hierarchy);
    // this.keys = Object.keys(this.LEVELS);

    // console.log(this.keys);
        // console.log(  this.LEVELS );

  }
  // data_hierarchy
  showsubLevel(data_hierarchy){
  //  alert(1)
    
    // this.devices =  data_hierarchy
    // console.log(  this.devices );
   
      this.sec =data_hierarchy['devices']
    //   // this.sec =data_hierarchy
    //   // console.log( this.sec );
      // this.navCtrl.push(TabPage,this.sec)
      if(this.sec.length >0){
        this.navCtrl.push(TabPage,this.sec)
      }else{
        this.sec =data_hierarchy
        //   // this.sec =data_hierarchy
        //   // console.log( this.sec );
          this.navCtrl.push(TabPage,this.sec)
      }
  

    }
    // console.log(data_hierarchy['devices']);
    // this.sec =data_hierarchy['devices']
  

  ionViewDidLoad() {
    // this.name = this.navParams.get("data_hierarchy").name;
    // this._id = this.navParams.get("data_hierarchy")._id;
    // console.log('ionViewDidLoad MenuPage'+this.LEVEL1);
    // console.log(this.data);
  }

}
