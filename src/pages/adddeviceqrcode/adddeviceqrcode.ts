import { Component } from '@angular/core';
import { NavController, NavParams, ToastController, ModalController } from 'ionic-angular';
import { AddDevicesPage } from '../add-devices/add-devices';
import { Hotspot } from '@ionic-native/hotspot';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
declare var WifiWizard;
/**
 * Generated class for the AdddeviceqrcodePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-adddeviceqrcode',
  templateUrl: 'adddeviceqrcode.html',
})
export class AdddeviceqrcodePage {
  id: any;
  pass: any;
  str: any;
  selectedProduct: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private toast: ToastController, private hotspot: Hotspot, private barcodeScanner: BarcodeScanner, public modalCtrl: ModalController, ) {
    this.AddDevice()
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AdddeviceqrcodePage');
  }
  go() {
    const modal = this.modalCtrl.create(AddDevicesPage);
    modal.present();
    // this.navCtrl.push(IframePage)
  }
  AddDevice() {
    // this.selectedProduct = {};
    this.barcodeScanner.scan().then((barcodeData) => {

      this.selectedProduct = barcodeData;

      console.log(this.selectedProduct.text.split(','))
      this.str = this.selectedProduct.text.split(',')

      var ssid = this.str[0].toString()
      var ssps = this.str[1].toString()
      this.id = atob(ssid)
      this.pass = atob(ssps)
      console.log(this.id);
      console.log(this.pass);

      const espwifi = {
        id: this.id,
        pass: this.pass
      }
      localStorage.setItem("espwifissid", this.id)
      localStorage.setItem("espwifipass", this.pass)
   
       this.hotspot.connectToWifiAuthEncrypt(this.id, this.pass, "OPEN", ["CCMP", "TKIP", "WEP40", "WEP104"]).then(data => {
        console.log('Connected to User');
       
        // display alert here
        // alert(data)
        let toast = this.toast.create({
          message:"change wifi ",
          duration: 2000
        });
        toast.present();
        this.go()
        toast.dismiss();
      }).catch(err =>
         { 
           
          let toast = this.toast.create({
            message:err,
            duration: 2000
          });
          toast.present();
          toast.dismiss();
          console.log(err) 
        });   

    }).catch(error => {
      alert(JSON.parse(error))
      console.log(error);
      console.log(error.error); // error message as string
      console.log(error.headers);

    });
  }
}
