import { Component } from '@angular/core';

/**
 * Generated class for the BlubComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'blub',
  templateUrl: 'blub.html'
})
export class BlubComponent {

  text: string;

  constructor() {
    
  }

}
