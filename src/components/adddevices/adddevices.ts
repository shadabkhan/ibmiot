import { Component } from '@angular/core';
import { AddDevicesPage } from '../../pages/add-devices/add-devices';
import { Hotspot } from '@ionic-native/hotspot';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import {  ToastController, ModalController } from 'ionic-angular';

/**
 * Generated class for the AdddevicesComponent component.
 *
 * See https://angular.io/api/core/Component for more info on Angular
 * Components.
 */
@Component({
  selector: 'adddevices',
  templateUrl: 'adddevices.html'
})
export class AdddevicesComponent {

  text: string;
  selectedProduct: any;
  str: any;
  id: string;
  pass: string;

  constructor(private toast: ToastController,private hotspot: Hotspot, private barcodeScanner: BarcodeScanner,public modalCtrl: ModalController) {
    this.AddDevice()
  }  go() {
    const modal = this.modalCtrl.create(AddDevicesPage);
    modal.present();
    // this.navCtrl.push(IframePage)
  }
  AddDevice() {
    // this.selectedProduct = {};
    this.barcodeScanner.scan().then((barcodeData) => {

      this.selectedProduct = barcodeData;

      console.log(this.selectedProduct.text.split(','))
      this.str = this.selectedProduct.text.split(',')

      var ssid = this.str[0].toString()
      var ssps = this.str[1].toString()
      this.id = atob(ssid)
      this.pass = atob(ssps)
      console.log(this.id);
      console.log(this.pass);

      const espwifi = {
        id: this.id,
        pass: this.pass
      }
      localStorage.setItem("espwifissid", this.id)
      localStorage.setItem("espwifipass", this.pass)

      this.hotspot.connectToWifiAuthEncrypt(this.id, this.pass, "OPEN", ["CCMP", "TKIP", "WEP40", "WEP104"]).then(data => {
        console.log('Connected to User');
       
        // display alert here
        // alert(data)
        let toast = this.toast.create({
          message:"change wifi ",
          duration: 2000
        });
        toast.present();
        this.go()
        toast.dismiss();
      }).catch(err =>
         { 
           
          let toast = this.toast.create({
            message:err,
            duration: 2000
          });
          toast.present();
          toast.dismiss();
          console.log(err) 
        });
      // WifiWizard.removeNetwork(s);

      // WifiWizard.connectNetwork(this.id, this.pass, this.faile);

      console.log("password", this.id, this.pass);
      /*  this.hotspot.removeWifiNetwork(ssid).then( () => {
            this.hotspot.connectToWifi(this.id, this.pass) 
                .then((data) => {
                      console.log(".........hotspot..........",data);
                 }, (error) => {
                      console.log(".........hotspot..........",error);
                 });
       }, () => {
       }); */
      // WifiWizard.connectNetwork(this.id, this.pass,this.win,this.faile)
      // this.getCurrentSSID();
      // .then(data =>{console.log(data)}).catch(err => {console.log(err)});
      // this.hotspot.removeWifiNetwork(this.ssid).then(data =>{console.log(data)}).catch(err => {console.log(err)});
      // this.hotspot.connectToWifi(this.id, this.pass).then(data =>{console.log(data)}).catch(err => {console.log(err)});
      /*    WifiWizard.addNetwork(wifi.id,wifi.pass).then(data =>{
           console.log(data)
         }).catch(err => console.log(err)); */
      /*   this.hotspot.removeWifiNetwork(this.id).then( () => {
          this.hotspot.connectToWifi(this.id, this.pass) 
              .then((data) => {
                    console.log(data);
               }, (error) => {
                    console.log(error);
               });
     }, (error) => {
      console.log(error);
  }); */
      /*   this.hotspot.connectToWifi(this.id, this.pass).then( conn =>{
          console.log(conn)
        }).catch(err =>{
          console.log(err)
  
        }) */
      /* this.hotspot.addWifiNetwork(this.id,"Open", this.pass).then( conn =>{
        console.log(conn)
      }).catch(err =>{
        console.log(err)

      }) */

      // var ssid = str[1].toString()
      //   if(this.selectedProduct !== undefined) {
      //     this.productFound = true;
      //   } else {
      //     this.productFound = false;
      //     this.toast.show(`Product not found`, '5000', 'center').subscribe(
      //       toast => {
 

      //       }
      //     );
      //   }
      // }, (err) => {
      //   this.toast.show(err, '5000', 'center').subscribe(
      //     toast => {
      //       console.log(toast);
      //     }
      //   );
    }) .catch(error => {
    alert(JSON.parse(error))
      console.log(error);
      console.log(error.error); // error message as string
      console.log(error.headers);
  
    });
  }
}
