import { NgModule } from '@angular/core';
import { AddorgComponent } from './addorg/addorg';
import { AddlevelComponent } from './addlevel/addlevel';
import { AdddevicesComponent } from './adddevices/adddevices';
@NgModule({
	declarations: [AddorgComponent,
    AddlevelComponent,
    AdddevicesComponent],
	imports: [],
	exports: [AddorgComponent,
    AddlevelComponent,
    AdddevicesComponent]
})
export class ComponentsModule {}
