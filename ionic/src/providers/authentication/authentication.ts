import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the AuthenticationProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AuthenticationProvider {
  apiUrl = 'http://linked-things-orgs.eu-gb.mybluemix.net/api/v1/users/';

  constructor(public http: HttpClient) {
    console.log('Hello AuthenticationProvider Provider');
  }
  signup(data) {
    return new Promise((resolve, reject) => {

      this.http.post(this.apiUrl + "signup", data)
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }
  signIn(data) {
    return new Promise((resolve, reject) => {

      this.http.post(this.apiUrl + "signin", data)
        .subscribe(data => {
          resolve(data);
        }, (err) => {
          reject(err);
        });
    });
  }
}
