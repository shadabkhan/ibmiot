import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
// import { MqttService, MqttMessage } from 'angular2-mqtt';
import { Observable } from 'rxjs';
import uuid from 'uuid/v1'; //here change 'v1' with the version you desire to use

import { Paho } from 'ng2-mqtt/mqttws31';
import { LoginPage } from '../login/login';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  message: any;
  client: Paho.MQTT.Client;
  id = this.guid();
  constructor(private nativePageTransitions: NativePageTransitions, public navCtrl: NavController) {
    this.id;
    this.connect();
    // this.clientSubscribe();
  }
  logout() {
    let options: NativeTransitionOptions = {
      direction: 'left',
      duration: 400,
      slowdownfactor: -1,
      iosdelay: 50
    };

    this.nativePageTransitions.slide(options);
    this.navCtrl.setRoot(LoginPage, {}, { animate: true, animation: 'transition', duration: 500, direction: 'forward' });
  }



  options = {
    userName: 'a-vhah7t-jsblah3dys',
    password: 'UGt)1NVCQnnfwV)CLE',
    timeout: 3,

    onSuccess: function () {
      this.task;

      console.log('connected');
    },
    //Gets Called if the connection could not be established            
    onFailure: function (message) {
      console.log('unable to connect to broker' + JSON.stringify(message));
    }
  };


  task = setInterval(() => {
    this.clientSubscribe();
  }, 3000);

  connect() {
    this.client = new Paho.MQTT.Client('vhah7t.messaging.internetofthings.ibmcloud.com', 1883, 'a:vhah7t:' + this.id);//     
    this.client.connect(this.options);
    this.client.onMessageArrived = (message: Paho.MQTT.Message) => {
      this.message = message.payloadString;
      console.log('Message arrived : ' + message.payloadString);
    };
    this.client.onConnectionLost = (responseObject: Object) => {
      console.log('Connection lost : ' + JSON.stringify(responseObject));
    };
  }




  clientSubscribe() {

    // console.log(this.client, "sasa");
    this.client.subscribe('iot-2/type/Server/id/+/evt/Events/fmt/json', '');
  }


  onConnected() {
    console.log("Connected");
    this.client.subscribe("123456", "0");
    // this.client.subscribe("123456");
    this.sendMessage('HelloWorld');
  }

  sendMessage(message: string) {
    let packet = new Paho.MQTT.Message(message);
    packet.destinationName = "123456";
    this.client.send(packet);
  }
  guid() {
    let date = new Date();

    let str = date.getFullYear() + '' + date.getMonth() + '' + date.getDate() + '' + date.getHours() + '' + date.getMinutes() + '' + date.getSeconds();
    console.log(str)
    return str;
  }
  // onMessage() {

  // }

  // onConnectionLost() {

  // }
}
