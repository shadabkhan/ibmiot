import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SignUpPage } from '../sign-up/sign-up';
import { HomePage } from '../home/home';
import { AuthenticationProvider } from '../../providers/authentication/authentication';
import { NativePageTransitions, NativeTransitionOptions } from '@ionic-native/native-page-transitions';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  data: any;
  _id: any;
  password: any;

  constructor(private nativePageTransitions: NativePageTransitions, public navCtrl: NavController, public navParams: NavParams, public AuthenticateProvider: AuthenticationProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }
  signUp() {
    let options: NativeTransitionOptions = {
      direction: 'left',
      duration: 400,
      slowdownfactor: -1,
      iosdelay: 50
    };

    this.nativePageTransitions.slide(options);
    this.navCtrl.setRoot(SignUpPage, {}, { animate: true, animation: 'transition', duration: 500, direction: 'forward' });
  }

  login() {
    const user = {
      _id: this._id,
      password: this.password,

      // imgPath:this.imgPaths

    }
    this.AuthenticateProvider.signIn(user).then(data => {
      console.log(data)
      localStorage.setItem('user', JSON.stringify(data));
      let options: NativeTransitionOptions = {
        direction: 'left',
        duration: 400,
        slowdownfactor: -1,
        iosdelay: 50
      };

      this.nativePageTransitions.slide(options);
      this.navCtrl.setRoot(HomePage, {}, { animate: true, animation: 'transition', duration: 500, direction: 'forward' });
      // this.navCtrl.setRoot(HomePage);



      // .then(data => {
      // data;
      //     console.log(data);
      //     this.data
      // })


    }).then(data => {
      // let loader = this.loadingCtrl.create({
      //       content: "Uploading..."
      //     });
      // loader.present();
      data;
      console.log(data);
      // loading.dismiss();

      this.data
      // let loading = this.loadingCtrl.create();
      // loading.present();
      // loader.dismiss();
      // this.navCtrl.setRoot(ProductcategoryPage);
      //     return false;
    }
      , (err) => {
        console.log(err);
        // loader.dismiss();
        // this.presentToast(err);
      });
  }
}
